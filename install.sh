#!/bin/bash

dev=$1

if [[ ! $(type -P mpfshell) ]]; then
        echo "mpfshell is not in your \$PATH!"
        exit
elif [[ $dev == "" ]]; then
        echo -e "Usage:\n\t./install.sh <dev>"
        echo -e "Example:\n\t./install.sh ttyUSB0"
        exit
elif [[ ! -w /dev/$dev ]]; then
        echo "/dev/$dev either does not exist or is not writable by you!"
        exit
fi

mpfshell -n -c "open $dev; put boot.py; put secrets.py; put config.py; put main.py; ls"
