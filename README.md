# esp-bootstrap

Bootstrap files for an ESP8266 running MicroPython

### Features

##### Automatic execution of 'user' app:
- Automatically calls `app.main()` on boot.
- Application you create for running on the ESP8266 should be placed in a file `app.py` on the device


##### Updater service:
- See the node below about security
- ESP8266 will listen for updates to the 'userspace' application, `app.py`, and when one is received it will write the new application (currently written to device as `app.py`) and automatically run it.
- Updates can be sent to the device, over a TCP connection, using the [esp-updater project](https://github.com/craftyguy/esp-updater)
  - If the esp-updater project is not used, updates should be sent to the device in the form `<32byte sha256><data>`, to the port specified in `main.py`. The first 32 bytes of the data received is presumed to be the sha256 checksum of the following data. If this checksum fails, no updates are applied.
- The old application (`app.py`), if one exists, is backed up on the device. At some point I'll add a way to restore the backup.


##### Wifi Connect:
- Will automatically connect to a configured Wifi network on boot
- Credentials separately in secrets.py and loaded by boot.py. See Installation below for details


************************************************
#### VERY IMPORTANT NOTE ABOUT SECURITY!!
The update mechanism can use a pre-shared passphrase to verify that the sender of the update is providing a matching passphrase on the device. This is accomplished by setting the `UPDATE_PASSPHRASE` variable in `secrets.py` (see "Installation" below), and by passing the passphrase to `esp_updater.py` when sending the update using the `-s` option.

The passphrase is verified by:
1) creating a sha256 hash of the passphrase in `secrets.py`
2) appending it to the update that is received
3) creating a sha256 hash of the update+passphrase hash, and comparing it to the hash that was recieved by the sender

**Note**: If `UPDATE_PASSPHRASE` is unset in `secrets.py`, then the authenticity of any updates recieved is questionable, and your esp8266 can/will execute any arbitrary code sent to it. So.. it's **strongly** recommended that this passphrase mechanism is used.


**Disclaimer**: I take no responsibility for your use of this software, nor make any promises about its security. Security is ultimately your responsibility.

************************************************


### Installation

Prerequisites:
- You need an ESP8266 device. This has been tested on the Wemos D1 Mini & Adafruit Huzzah.
- The ESP8266 must be running MicroPython. 

Copy `boot.py`, `main.py`, and `config.py` to the device's flash. I recommend using [mpfshell](https://github.com/wendlers/mpfshell) to do this. To enable relevant messages from the updater or during boot (e.g. on wifi connect), edit `config.py` and set `SILENT = False`. To set a static IP addres, uncomment and edit the variables `IP`, `SUBNET`, `GATEWAY` and `DNS`.

Create a `secrets.py` file in the following format, and copy to the device's flash:
```
WIFI_SSID = "<put SSID here>"
WIFI_PASSPHRASE = "<put super secret passphrase here>"
UPDATE_PASSPHRASE = "<put super secret passphrase here for validating authenticity of update>"
```

Place your application file, named as `app.py`, on the device's flash. This application must implement a `main()` method.

Upon first boot, if `app.py` does not yet exist, the device will boot to repl. You can use the host-side `esp_updater.py` application ([found here](https://github.com/craftyguy/esp-updater)) to send updates to `app.py` to the ESP8266.
