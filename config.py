# If set, writing to serial console will be minimized
SILENT = True

# Set port for update service to listen on.
LISTEN_PORT = const(1337)

# Set the values below to initiate the network interface with a static IP.
#IP = "192.168.0.123"
#SUBNET = "255.255.255.0" # aka Netmask
#GATEWAY = "192.168.0.1"
#DNS = "8.8.8.8"
